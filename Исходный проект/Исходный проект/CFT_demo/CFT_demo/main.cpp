#include <iostream>
#include <fstream>

using namespace std;

//Read from input file integer array
void read(long long  *mass, char* file, int &size){
	ifstream in;
	const char ch = '\n';
	char str[255] = "";

	in.open(file);
	if (!in.is_open()) throw 5;
	
	while (!in.eof()){
		if (size == 100) throw 8;
		in.getline(str, 255, ch);

		//If string isn't empty
		if (str[0]) mass[size++] = atoll(str); 
		}
	in.close();
	if (size == 0) throw 7;
}

//Read from input file string array
void read(char mass[100][255], char* file, int &size){
	ifstream in;
	const char ch = '\n';
	char str[255] = "";

	in.open(file);
	if (!in.is_open()) throw 5;


	while (!in.eof()){
		if (size == 100) throw 8;
		in.getline(str, 255, ch);
		////If size of string is more then second parameter getline - need to reset fail bit
		if ((str[0]) && (in.fail())) in.clear();

		//If string isn't empty
		if (str[0]) strcpy(mass[size++], str);
	}
	in.close();
	if (size == 0) throw 7;
}

//Compare integer
bool cmp(int a, int b){
	return a > b;
}

//Compare string
bool cmp(char *a, char*b){
	if (strcmp(a, b)==1) return 1;
	return 0;
}


//Swaps the data
template <typename T>
void swap(T **a, T **b){
	T *tmp = *a;
	*a = *b;
	*b = tmp;
}

//Sorting the data with Insertion sort
template <typename T>
void sort(T *mass, int size){
	for (int i = 1; i < size; i++)
	for (int j = i; cmp(mass[j - 1], mass[j]) && j>0; j--)
		swap(mass[j-1], mass[j]);
}

//Write data to output file
template <typename T>
void write(T *mass, char* file, char type, int size){
	ofstream out;
	out.open(file);
	if (!out.is_open()) throw 6; //If impossible to open file
	switch (type){
	case 'a': //ascending
		for (int i = 0; i < size; i++)
			out << mass[i] << endl;
		break;
	case 'd'://descending
		for (int i = size - 1; i >= 0; i--)
			out << mass[i] << endl;
		break;
	default: throw 4; break;
	}
	out.close();
}


int main(int argc, char* argv[]){
	int size = 0; //count of input strings
	
	try {
		//Check the count of arguments
		if (argc != 5) throw 1;
		//Check the arguments
		if ((argv[3][0] != '-') || (argv[4][0] != '-') || (strlen(argv[3]) != 2) || (strlen(argv[4]) != 2)) throw 2;

		//Choose the type of data
		switch (argv[3][1]){
		case 'i': 
			long long mass[100]; 
			read(mass, argv[1], size);
			sort(mass, size);
			write(mass, argv[2], argv[4][1], size);
			break;
		case 's': 
			char mas[100][255];
			read(mas, argv[1], size);
			sort(mas, size);
			write(mas, argv[2], argv[4][1], size);
			break;
		default: throw 3; break;
		}

	}
	catch (int i) {
		switch (i){
		case 1: cout << "Number of arguments must have be 4\nFor example:\nsort_it.exe in.txt out.txt -i -a"; break;
		case 2: cout << "Arguments aren't correct.\nYou must write:\n<Name of programm> <Input file path> <Output file path> <Type of input data> '-s' for string, '-i' for integer\n"
			<< "<Type of sorting> '-a' for ascending,'-d' for descending\nFor example:\nsort_it.exe in.txt out.txt -i -a"; break;
		case 3: cout << "Argument of data type isn't correct. Use '-s' for string or '-i' for integer"; break;
		case 4: cout << "Argument of sorting type isn't correct. Use '-a' for ascending or '-d' for descending"; break;
		case 5: cout << "Cann't read file - " << argv[1] << ". Make sure a file with this name exists."; break;
		case 6: cout << "Impossible to create file - " << argv[2]; break;
		case 7: cout << "File is empty - " << argv[1]; break;
		case 8: cout << "Input data is over then 100 strings. Program uses only first 100."; break;

		default: cout << "Exception"; break;
		}
	}

}